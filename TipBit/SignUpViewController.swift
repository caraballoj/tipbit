//
//  SignUpViewController.swift
//  TipBit
//
//  Created by Jaxs on 7/7/16.
//  Copyright © 2016 TipBit. All rights reserved.
//

import UIKit
import SwiftLoader
import Alamofire

class SignUpViewController: UIViewController, UITextFieldDelegate {

    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstName.delegate = self
        lastName.delegate = self
        email.delegate = self
        password.delegate = self
        confirmPassword.delegate = self
        
        firstName.layer.borderColor = TipBitContstants.Appearance.Colors.RichTealColor.CGColor
        firstName.layer.masksToBounds = true
        firstName.layer.borderWidth = 1.0
        firstName.text = "First Name"

        lastName.layer.borderColor = TipBitContstants.Appearance.Colors.RichTealColor.CGColor
        lastName.layer.masksToBounds = true
        lastName.layer.borderWidth = 1.0
        lastName.text = "Last Name"

        email.layer.borderColor = TipBitContstants.Appearance.Colors.RichTealColor.CGColor
        email.layer.masksToBounds = true
        email.layer.borderWidth = 1.0
        email.text = "Email"
        
        password.layer.borderColor = TipBitContstants.Appearance.Colors.RichTealColor.CGColor
        password.layer.masksToBounds = true
        password.layer.borderWidth = 1.0
        password.text = "Password"

        confirmPassword.layer.borderColor = TipBitContstants.Appearance.Colors.RichTealColor.CGColor
        confirmPassword.layer.masksToBounds = true
        confirmPassword.layer.borderWidth = 1.0
        confirmPassword.text = "Confirm Password"

        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
        
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        let keyboardHeight = keyboardRectangle.height
        bottomLayoutConstraint.constant = keyboardHeight + 10.0
        self.updateView()
    }
    
    func keyboardWillHide(notification:NSNotification) {
        bottomLayoutConstraint.constant = 31.0
        self.updateView()
    }
    
    private func updateView() {
        UIView.animateWithDuration(0.24) {
            self.logoImage.alpha = self.bottomLayoutConstraint.constant == 31.0 ? 1:0
            self.view.layoutIfNeeded()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signupBtnTapped(sender: UIButton) {
        
        if firstName.text != "First Name" && firstName.text != " " && lastName.text != "Last Name" && lastName.text != " " && email.text != "Email" && email.text != " " && password.text != "Password" && password.text != " " && confirmPassword.text != "Confirm Password" && confirmPassword.text != " " {
            
            if password.text != confirmPassword.text{
                showAlertWithOKButton("Alert", message: "Please Confirm Password.", presentingViewController: self)
            }else{
                SwiftLoader.show(animated: true)
                
                Alamofire.request(Router.RegProfile(self.firstName.text!, self.lastName.text!, self.email.text!, self.password.text!, self.confirmPassword.text!)).responseObject {
                    
                    (response: Response<RegProfile, NSError>) in
                    debugPrint(response)
                    
                    
                    switch response.result {
                    case .Success:
                        let value = response.result.value
                        let signupStatus = value!.status
                        print("status: \(signupStatus)")
                        
                        if signupStatus == "ok"{
                            print("server signup status ok")
                            
                            //GO TO NEXT VIEW CONTROLLER
                            
                        }else{
                            print("status not ok")
                            let value = response.result.value
                            let errorMessage = value!.error_message
                            let errorMessages = value!.error_messages
                            
                            //FIX
                            //retrieving error message from server
                            if errorMessage != "" {
                                showAlertWithOKButton(errorMessage, message: "", presentingViewController: self)
                            }else if errorMessages != "" {
                                showAlertWithOKButton(errorMessages, message: "", presentingViewController: self)
                            }
                        }
                        
                    case .Failure(let error):
                        print(error)
                    }
                }
                SwiftLoader.hide()
            }
        }else{
            showAlertWithOKButton("Alert", message: "Please check all text fields.", presentingViewController: self)
        }
    }

    @IBAction func backBtnTapped(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    //MARK - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.text = ""
        if textField == self.password || textField == self.confirmPassword{
            textField.secureTextEntry = true
        }
    }

    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if firstName.text != "First Name" && firstName.text != " " && lastName.text != "Last Name" && lastName.text != " " && email.text != "Email" && email.text != " " && password.text != "Password" && password.text != " " && confirmPassword.text != "Confirm Password" && confirmPassword.text != " " {
            
            //FIX
            // setTitleColor not working
            //signupBtn.setTitleColor(UIColor.greenColor(), forState: UIControlState.Highlighted)
            //signupBtn.tintColor = UIColor.greenColor()
            
        }else{
            if firstName.text == ""{
                firstName.text = "First Name"
            }
            
            if lastName.text == ""{
                lastName.text = "Last Name"
            }
            
            if email.text == ""{
                email.text = "Email"
            }
            
            if password.text == ""{
                password.secureTextEntry = false
                password.text = "Password"
            }
            
            if confirmPassword.text == ""{
                confirmPassword.secureTextEntry = false
                confirmPassword.text = "Confirm Password"
            }
        }
        return true
    }
    

   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
