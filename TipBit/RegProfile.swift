//
//  RegProfile.swift
//  TipBit
//
//  Created by Jaxs on 7/11/16.
//  Copyright © 2016 TipBit. All rights reserved.
//

import UIKit

// see: http://stackoverflow.com/questions/28490129/alamofire-nested-json-serializer

final class RegProfile: ResponseObjectSerializable {
    let status: String
    let error_message: String
    let error_messages: String
    var error_password: String
    var error_email: String
    let email: String
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        print(representation)
        self.status = representation.valueForKeyPath("status") as? String ?? ""
        self.error_message = representation.valueForKeyPath("error_message") as? String ?? ""
        self.error_messages = representation.valueForKeyPath("error_messages") as? String ?? ""
        self.email = representation.valueForKeyPath("email") as? String ?? ""
        
        
        //FIX
        let messages = representation["error_messages"]
        self.error_password = messages!!.valueForKeyPath("password") as? String ?? ""
        self.error_email = messages!!.valueForKeyPath("email") as? String ?? ""

        var passwordMess : Password?
        
        if let errMessages: AnyObject = representation.valueForKeyPath("error_messages") {
            
            let password = representation.valueForKeyPath("password") as? String
            
            passwordMess   = Password(response: response, representation: errMessages)
        }
        
        print(passwordMess )

        var emailMess : Email?
        
        if let errMessages: AnyObject = representation.valueForKeyPath("error_messages") {
            emailMess   = Email(response: response, representation: errMessages)
        }
        print(emailMess )

        
       
    }

}


final class Password : ResponseObjectSerializable, CustomStringConvertible {
    
    let password: String
    
    var description: String {
        return "error_messages {password = \(self.password) }"
    }
    
    init(password: String ) {
        self.password = password
     }
    
    required init?(response: NSHTTPURLResponse, representation: AnyObject) {
         let password = representation.valueForKeyPath("password") as? String
        
        if password! == ""  {
            self.password = ""
            return nil

        } else {
            self.password = password!

        }
    }
}

final class Email : ResponseObjectSerializable, CustomStringConvertible {
    
    let email: String
    
    var description: String {
        return "error_messages {email = \(self.email) }"
    }
    
    init(email: String ) {
        self.email = email
    }
    
    required init?(response: NSHTTPURLResponse, representation: AnyObject) {
        let email = representation.valueForKeyPath("email") as? String
        
        if email!.isEmpty   {
            self.email = email!
        } else {
            self.email = ""
            return nil
        }
    }
}

 