//
//  Router.swift
//  TipBit
//
//  Created by Jaxs on 7/7/16.
//  Copyright © 2016 TipBit. All rights reserved.
//

import Alamofire

enum Router: URLRequestConvertible {
    
    static let baseURLString = "http://staging.tipbit.io/api/v1/"
    static var OAuthToken: String?
    
    case LogIn(String, String!)
    case RegProfile(String, String, String, String, String!)
    case ReferFriend([String: AnyObject])
    case BetaSignup([String: AnyObject])
    
    var method: Alamofire.Method {
        switch self {
        case .LogIn, .RegProfile, .ReferFriend, .BetaSignup:
            return .POST
        }
    }
    
    var path: String {
        switch self {
        case .LogIn:
            return "login/signin.json/"
        case .RegProfile:
            return "login/signup.json/"
        case .ReferFriend:
            return "refer_friend/"
        case .BetaSignup:
            return "beta_signup/"
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        let URL = NSURL(string: Router.baseURLString)!
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
        mutableURLRequest.HTTPMethod = method.rawValue
 
        switch self {
        case .LogIn(let email, let password) where password != nil:
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: ["email": email, "password": password, "api_key": TipBitContstants.apiKey, "api_secret": TipBitContstants.apiSecret]).0
            
        case .LogIn(let email, _):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: ["email_address": email]).0
            
        case .RegProfile(let firstname, let lastname, let email, let password, let  passwordConfirm):
            
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: ["firstname" : firstname, "lastname" : lastname, "email" : email, "password" : password, "password_confirmation" : passwordConfirm, "api_key": TipBitContstants.apiKey, "api_secret": TipBitContstants.apiSecret]).0
            
        case .ReferFriend(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: ["parameters": parameters]).0
            
        case .BetaSignup(let parameters):
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: ["parameters": parameters]).0
        }
    }

}
