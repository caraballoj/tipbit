//
//  LoginViewController.swift
//  TipBit
//
//  Created by Jaxs on 7/7/16.
//  Copyright © 2016 TipBit. All rights reserved.
//

import UIKit
import Alamofire
import SwiftLoader


class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        email.delegate = self
        password.delegate = self
        
        email.layer.borderColor = TipBitContstants.Appearance.Colors.RichTealColor.CGColor
        email.layer.masksToBounds = true
        email.layer.borderWidth = 1.0
        
        password.layer.borderColor = TipBitContstants.Appearance.Colors.RichTealColor.CGColor
        password.layer.masksToBounds = true
        password.layer.borderWidth = 1.0

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)

    }

    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        let keyboardHeight = keyboardRectangle.height
        bottomLayoutConstraint.constant = keyboardHeight + 10.0
        self.updateView()
    }
    
    func keyboardWillHide(notification:NSNotification) {
        bottomLayoutConstraint.constant = 122.0
        self.updateView()
    }
    
    private func updateView() {
        UIView.animateWithDuration(0.24) {
            self.logoImage.alpha = self.bottomLayoutConstraint.constant == 122.0 ? 1:0
            self.view.layoutIfNeeded()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnTapped(sender: UIButton) {
        
        if email.text == "" || password.text == ""{
            showAlertWithOKButton("Alert", message: "Enter email and password.", presentingViewController: self)
        }else{
            SwiftLoader.show(animated: true)
            Alamofire.request(Router.LogIn(self.email.text!, self.password.text!)).responseObject {
                (response: Response<LogIn, NSError>) in
                debugPrint(response)
                SwiftLoader.hide()
                switch response.result {
                case .Success:
                    let value = response.result.value
                    let token = value!.user_token
                    print("token: \(token)")
                    
                case .Failure(let error):
                    print(error)
                }
            }
        }
        
    }

    @IBAction func backBtnTapped(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)

    }
    
    //MARK - UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
