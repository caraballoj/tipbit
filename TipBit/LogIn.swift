//
//  LogIn.swift
//  TipBit
//
//  Created by Jaxs on 7/7/16.
//  Copyright © 2016 TipBit. All rights reserved.
//

import UIKit

final class LogIn: ResponseObjectSerializable {
    let status: String
    let user_token: String
    let user_token_expires_at: String
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        print(representation)
        self.status = representation.valueForKeyPath("status") as? String ?? ""
        self.user_token = representation.valueForKeyPath("user_token") as? String ?? ""
        self.user_token_expires_at = representation.valueForKeyPath("user_token_expires_at") as? String ?? ""
    }
}